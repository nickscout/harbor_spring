package com.epam.nickscout.harbors;

import com.epam.nickscout.harbors.model.Ship.Ship;
import com.epam.nickscout.harbors.model.harbor.Harbor;
import com.epam.nickscout.harbors.model.harbor.HarborManager;
import com.epam.nickscout.harbors.model.harbor.HarborsStraightRoute;
import com.epam.nickscout.harbors.repository.HarborRepository;
import com.epam.nickscout.harbors.repository.HarborsStraightRoutesRepository;
import com.epam.nickscout.harbors.repository.ShipRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class HarborsApplication {

    @Autowired
    private HarborRepository harborRepository;

    @Autowired
    private HarborsStraightRoutesRepository harborsStraightRoutesRepository;

    @Autowired
    private ShipRepository shipRepository;


    @PostConstruct
    public void onStart(){
        Set <Harbor> harbors = saveBlackSeaHarbors();
        saveBlackSeaHarborRoutes(harbors);
        Harbor Odessa = harbors.stream().filter(harbor -> harbor.getName().equals("Odessa")).findFirst().get();
        Harbor Nvr = harbors.stream().filter(harbor -> harbor.getName().equals("Novorossiysk")).findFirst().get();

        Ship ship = new Ship();
        ship.setName("Avrora");
        ship.setPowerReserve(999999);
        ship.setSinking(5);
        ship.setTonnage(99);

        HarborManager.getInstance().setHarborRepository(harborRepository);
        HarborManager.getInstance().setHarborsStraightRoutesRepository(harborsStraightRoutesRepository);

        var route = HarborManager.getInstance().getRoute(Odessa, Nvr, ship);
    }

    public Set<Harbor> saveBlackSeaHarbors() {
        Harbor sevastopol = new Harbor();
        sevastopol.setName("Sevastopol");
        sevastopol.setFuelCost(999);
        sevastopol.setDepth(100);

        Harbor odessa = new Harbor();
        odessa.setName("Odessa");
        odessa.setDepth(50);
        odessa.setFuelCost(150);

        Harbor stambul = new Harbor();
        stambul.setName("Stambul");
        stambul.setFuelCost(50);
        stambul.setDepth(50);

        Harbor novorossiysk = new Harbor();
        novorossiysk.setName("Novorossiysk");
        novorossiysk.setDepth(70);
        novorossiysk.setFuelCost(120);

        Set <Harbor> harbors = new HashSet<>();
        harbors.add(sevastopol);
        harbors.add(odessa);
        harbors.add(stambul);
        harbors.add(novorossiysk);

        harborRepository.saveAll(harbors);
        return harbors;
    }

    public void saveBlackSeaHarborRoutes(Set <Harbor> harbors) {
        HarborsStraightRoute routeSevastopolOdessa = new HarborsStraightRoute();
        routeSevastopolOdessa.setHarborA(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Sevastopol"))
                .findAny()
                .get());
        routeSevastopolOdessa.setHarborB(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Odessa"))
                .findAny()
                .get());
        routeSevastopolOdessa.setDistance(304);

        HarborsStraightRoute routeSevastopolStambul = new HarborsStraightRoute();
        routeSevastopolStambul.setHarborA(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Sevastopol"))
                .findAny()
                .get());
        routeSevastopolStambul.setHarborB(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Stambul"))
                .findAny()
                .get());
        routeSevastopolStambul.setDistance(544);

        HarborsStraightRoute routeOdessaStambul = new HarborsStraightRoute();
        routeOdessaStambul.setHarborA(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Odessa"))
                .findAny()
                .get());
        routeOdessaStambul.setHarborB(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Stambul"))
                .findAny()
                .get());
        routeOdessaStambul.setDistance(620);

        HarborsStraightRoute routeStambulNovorossiysk = new HarborsStraightRoute();
        routeStambulNovorossiysk.setHarborA(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Stambul"))
                .findAny()
                .get());
        routeStambulNovorossiysk.setHarborB(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Novorossiysk"))
                .findAny()
                .get());
        routeStambulNovorossiysk.setDistance(829);

        HarborsStraightRoute routeSevastopolNovorossiysk = new HarborsStraightRoute();
        routeSevastopolNovorossiysk.setHarborA(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Sevastopol"))
                .findAny()
                .get());
        routeSevastopolNovorossiysk.setHarborB(harbors.stream()
                .filter(harbor -> harbor.getName().equals("Novorossiysk"))
                .findAny()
                .get());
        routeSevastopolNovorossiysk.setDistance(344);

        Set<HarborsStraightRoute> routes = new HashSet<>();
        routes.add(routeSevastopolOdessa);
        routes.add(routeSevastopolStambul);
        routes.add(routeOdessaStambul);
        routes.add(routeStambulNovorossiysk);
        routes.add(routeSevastopolNovorossiysk);

        harborsStraightRoutesRepository.saveAll(routes);
    }

	public static void main(String[] args) {
		SpringApplication.run(HarborsApplication.class, args);
	}

}
