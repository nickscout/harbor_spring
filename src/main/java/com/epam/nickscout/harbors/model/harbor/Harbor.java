package com.epam.nickscout.harbors.model.harbor;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Data
public class Harbor {
    @Column @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.PRIVATE)
    long id;
    String name;
    int depth;
    int fuelCost;

}
