package com.epam.nickscout.harbors.model.harbor;

import com.epam.nickscout.harbors.model.Ship.Ship;
import com.epam.nickscout.harbors.repository.HarborRepository;
import com.epam.nickscout.harbors.repository.HarborsStraightRoutesRepository;
import org.jgrapht.Graph;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;


public class HarborManager {

    @Autowired
    private HarborManager initialInstance;
    private static HarborManager ourInstance;

    public static HarborManager getInstance() {
        return  ourInstance;
    }

    @PostConstruct
    public void init() {
        ourInstance =  initialInstance;
        buildGraph();
    }

    @Autowired
    private   HarborsStraightRoutesRepository harborsStraightRoutesRepository;
    @Autowired
    private   HarborRepository harborRepository;
    private  Graph<Harbor, Edge> graph;
    private  Optional<Ship> shipCursor = Optional.empty();

    public Optional<Ship> getShipCursor() {
        return shipCursor;
    }

    private  void setShip(Ship ship) {
        shipCursor = Optional.of(ship);
    }

    private  void revokeShip() {
        shipCursor = Optional.empty();
    }

    private  void buildGraph() {

        graph = new DefaultUndirectedWeightedGraph<>(Edge.class);
        harborRepository.findAll().forEach(harbor -> graph.addVertex(harbor));
        harborsStraightRoutesRepository.findAll()
                .forEach(harborsStraightRoute -> graph.addEdge(
                        harborsStraightRoute.getHarborA(),
                        harborsStraightRoute.getHarborB(),
                        new Edge(harborsStraightRoute)
                        ));
    }

    public List<Harbor> getRoute(Harbor harborA, Harbor harborB, Ship ship) {
        List<Harbor> route;
        try {
            getInstance().setShip(ship);

            DijkstraShortestPath<Harbor, Edge> shortestPath = new DijkstraShortestPath<>(graph);
            route = shortestPath.getPath(harborA, harborB).getVertexList();

        } finally {
            getInstance().revokeShip();
        }
        return route;

    }

}

class Edge extends DefaultWeightedEdge {
    private HarborsStraightRoute route;

    public Edge(HarborsStraightRoute route) {
        super();
        this.route = route;
    }

    public Edge(HarborsStraightRoute route, Ship ship) {
        this.route = route;
    }

    @Override
    protected Object getSource() {
        return route.getHarborA();
    }

    @Override
    protected Object getTarget() {
        return route.getHarborB();
    }

    @Override
    protected double getWeight() {
        Optional<Ship> shipCursor = HarborManager.getInstance().getShipCursor();
        if (shipCursor.isPresent()) {
            if (shipCursor.get().canReach(route)) {
                return route.getDistance();
            }
            else {
                return Integer.MAX_VALUE;
            }
        } else {
            return route.getDistance();
        }
    }
}