package com.epam.nickscout.harbors.model.Ship;

import com.epam.nickscout.harbors.model.harbor.HarborsStraightRoute;

public interface Reachable {
    boolean canReach(HarborsStraightRoute harborsStraightRoute);
}
