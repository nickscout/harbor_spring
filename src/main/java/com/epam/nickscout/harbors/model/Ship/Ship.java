package com.epam.nickscout.harbors.model.Ship;

import com.epam.nickscout.harbors.model.harbor.Harbor;
import com.epam.nickscout.harbors.model.harbor.HarborsStraightRoute;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Data
public class Ship implements Fitable, Reachable {

    @Column(unique = true)
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.PRIVATE)
    private long id;
    private double tonnage;
    private double sinking;
    private int powerReserve;
    private String name;

    @Override
    public boolean canFit(Harbor harbor) {
        return sinking < harbor.getDepth();
    }

    @Override
    public boolean canReach(HarborsStraightRoute harborsStraightRoute) {
        return powerReserve < harborsStraightRoute.getDistance();
    }
}
