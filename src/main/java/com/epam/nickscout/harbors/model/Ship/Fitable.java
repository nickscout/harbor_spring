package com.epam.nickscout.harbors.model.Ship;

import com.epam.nickscout.harbors.model.harbor.Harbor;

public interface Fitable {
    boolean canFit(Harbor harbor);
}
