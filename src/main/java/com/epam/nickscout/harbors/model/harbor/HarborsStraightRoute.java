package com.epam.nickscout.harbors.model.harbor;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;

@Data
@Entity
public class HarborsStraightRoute {
    @OneToOne
    Harbor harborA;
    @OneToOne
    Harbor harborB;
    int distance;
    @Column @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.PRIVATE)
    long id;


}
