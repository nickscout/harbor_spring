package com.epam.nickscout.harbors.controller.rest;

import com.epam.nickscout.harbors.model.harbor.Harbor;
import com.epam.nickscout.harbors.repository.HarborRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class HarborController {

    @Autowired
    HarborRepository harborRepository;

    @GetMapping(value = "/harbor/{id}")
    public ResponseEntity<Harbor> getHarborById(@PathVariable Long id) throws Exception {
        Optional<Harbor> optionalHarbor = (harborRepository.findById(id));
        if (optionalHarbor.isPresent()) {
            return new ResponseEntity<>(optionalHarbor.get(), HttpStatus.OK);
        } else throw new Exception("Not found");
    }

    @GetMapping(value = "/harbor/")
    public ResponseEntity<List<Harbor>> getHarborById() {
        return new ResponseEntity<>(harborRepository.findAll(), HttpStatus.OK);

    }

    @PostMapping(value = "/harbor/")
    public ResponseEntity<Harbor> addHarbor(@RequestBody Harbor harbor) {
        harborRepository.save(harbor);
        return new ResponseEntity<>(harbor, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/harbor/{id}")
    public HttpStatus deleteHarbor(@PathVariable Long id) {
        harborRepository.deleteById(id);
        return HttpStatus.OK;
    }
}
