package com.epam.nickscout.harbors.repository;

import com.epam.nickscout.harbors.model.Ship.Ship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShipRepository extends JpaRepository<Ship, Long> {
}
