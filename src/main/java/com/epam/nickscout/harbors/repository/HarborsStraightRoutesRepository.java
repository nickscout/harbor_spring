package com.epam.nickscout.harbors.repository;

import com.epam.nickscout.harbors.model.harbor.HarborsStraightRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HarborsStraightRoutesRepository extends JpaRepository<HarborsStraightRoute, Long> {
}
