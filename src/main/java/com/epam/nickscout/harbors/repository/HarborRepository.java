package com.epam.nickscout.harbors.repository;

import com.epam.nickscout.harbors.model.harbor.Harbor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HarborRepository extends JpaRepository<Harbor, Long> {
}
